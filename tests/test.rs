use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use search_autocompletion::AutoComplete;

#[test]
fn works() {
    let imp: Vec<(String, isize)> = vec![
        ("Ahoj".to_string(), 10),
        ("Ano".to_string(), -1),
        ("Anarchie".to_string(), 4),
        ("Bobule".to_string(), 24),
        ("Beton".to_string(), -14),
    ];
    let mut v = AutoComplete::new(&imp);
    v.insert(&("Alkohol", 7));

    assert_eq!(v.get_strings_for_prefix("A").unwrap(), vec!["Ahoj", "Alkohol", "Anarchie", "Ano"]);
}

#[test]
fn breaks() {
    let v = AutoComplete::<isize>::default();

    assert_eq!(v.get_strings_for_prefix("AHAHAHHA"), None)
}

#[test]
fn change_weight() {
    let mut v = AutoComplete::default();
    v.insert(&("Alkohol", 7));

    assert!(v.change_weight(&("Alkohol", 11)).is_ok());
    assert!(v.change_weight(&("Alkohoddd", 11)).is_err());
}

#[test]
fn bench() {
    let file = File::open("words_alpha.txt").unwrap();
    let reader = BufReader::new(file);
    let mut v: AutoComplete<isize> = AutoComplete::default();

    for line in reader.lines() {
        v.insert(&(&line.unwrap(), 0));
    }
    v.insert(&("Hello how are you? Arisa", 0));

    assert_eq!(v.get_strings_for_prefix("Hello how are ").unwrap(), vec!["Hello how are you? Arisa"]);
    assert_eq!(
        v.get_strings_for_prefix("zom").unwrap(),
        vec![
            "zombi",
            "zombie",
            "zombielike",
            "zombies",
            "zombiism",
            "zombiisms",
            "zombis",
            "zomotherapeutic",
            "zomotherapy"
        ]
    );
}
