//!A Rust implementation of Search Auto Completion
//! # Examples
//! ```
//! use search_autocompletion::AutoComplete;
//!
//! let mut com = AutoComplete::default();
//! com.insert(&("Hello", 9));
//! com.insert(&("Hell", 10));
//! com.insert(&("Ham", 1000));
//! com.insert(&("Hen", 54));
//!
//! let strings = com.get_strings_for_prefix("He").unwrap();
//! assert_eq!(strings, vec!["Hen", "Hell", "Hello"]);
//! ```
#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![deny(missing_docs)]
use std::collections::BTreeMap;
use unicode_segmentation::UnicodeSegmentation;

/// The [`AutoComplete`] struct, basically wrapper around [`Node`]
#[derive(Default)]
pub struct AutoComplete<T: PartialOrd + Ord + Clone + Default> {
    trie: Node<T>,
}

impl<T> AutoComplete<T>
where
    T: PartialOrd + Ord + Clone + Default,
{
    /// Create a new [`AutoComplete`] with pre-defined strings to autocomplete
    pub fn new(dict: &[(String, T)]) -> Self {
        let mut trie = Self::default();
        dict.iter().for_each(|v| trie.insert(&(&v.0, v.1.clone())));
        trie
    }

    /// Insert a string into the [`AutoComplete`] struct to autocomplete it
    pub fn insert(&mut self, value: &(&str, T)) {
        let mut curr = &mut self.trie;
        let graphemes = UnicodeSegmentation::graphemes(value.0, true).collect::<Vec<&str>>();
        let l = graphemes.len();
        for (c, v) in graphemes.iter().enumerate() {
            let v = v.to_string();
            curr = curr
                .children
                .entry(v.clone())
                .and_modify(|f| {
                    if c == (l - 1) {
                        f.weight = value.1.clone();
                        f.is_word = true;
                    }
                })
                .or_insert_with(|| Node::new(graphemes[0..=c].concat(), c == (l - 1), value.1.clone()));
        }
    }

    /// Change weight of some string
    /// # Errors
    /// If Result is not OK then it failed to set the weight for this specific string for some reason
    pub fn change_weight(&mut self, value: &(&str, T)) -> Result<(), String> {
        let mut curr = &mut self.trie;

        let graphemes = UnicodeSegmentation::graphemes(value.0, true).collect::<Vec<&str>>();
        for v in graphemes.into_iter() {
            curr = match curr.children.get_mut(v) {
                Some(a) => a,
                None => return Err(format!("Failed to set the weight for {}!", value.0)),
            };
        }
        curr.weight = value.1.clone();
        Ok(())
    }

    /// Get strings that fit the prefix
    pub fn get_strings_for_prefix(&self, prefix: &str) -> Option<Vec<String>> {
        let mut results: Vec<(String, T)> = Vec::new();
        let mut curr = &self.trie;

        let graphemes = UnicodeSegmentation::graphemes(prefix, true);
        for v in graphemes {
            curr = match curr.children.get(v) {
                Some(a) => a,
                None => return None,
            }
        }

        Self::find_all_child_words(curr, &mut results);
        results.sort_by(|(_, a), (_, b)| b.cmp(a));
        Some(results.into_iter().map(|(v, _)| v).collect())
    }

    /// Recursive function that searches for all children of a Vector of strings
    fn find_all_child_words(node: &Node<T>, results: &mut Vec<(String, T)>) {
        if node.is_word {
            results.push((node.prefix.clone(), node.weight.clone()));
        }

        node.children.keys().for_each(|v| Self::find_all_child_words(node.children.get(v).unwrap(), results));
    }
}

/// Node that is used in [`AutoComplete`] struct
#[derive(Default)]
struct Node<T>
where
    T: PartialOrd + Ord + Default + Clone,
{
    prefix: String,
    children: BTreeMap<String, Node<T>>,
    is_word: bool,
    weight: T,
}

impl<T> Node<T>
where
    T: PartialOrd + Ord + Default + Clone,
{
    /// Creates a new node with a prefix and indicator if it is a word
    fn new(prefix: String, is_word: bool, weight: T) -> Self {
        Self {
            prefix,
            children: BTreeMap::new(),
            is_word,
            weight: if is_word { weight } else { T::default() },
        }
    }
}
